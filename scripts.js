"use strict";

const FIELD_WIDTH = 16;
const FIELD_HEIGHT = 16;
const BOMBS_AMOUNT = 50;

let field = document.querySelector("div.field");

let cells = [];

let isClicked = false;

InitGameField(field, cells);

document.onclick = function(event){
  if(!event.target.classList.contains("cell-closed")) return;

  if(!isClicked) SpawnBombs(event.target);

  OpenCell(event.target);
};

function OpenCell(cell){
  cell.classList.remove("cell-closed");
  cell.classList.add("cell-opened");

  if(cell.hasBomb) {
    cell.innerText = '*';
    return;
  };

  cell.innerText = cell.number;
};

function SpawnBombs(target){

  let count = BOMBS_AMOUNT;

  while(count > 0){
    let x = Math.floor(Math.random() * FIELD_WIDTH);
    let y = Math.floor(Math.random() * FIELD_HEIGHT);

    if(cells[y][x].hasBomb || (target.x == x && target.y == y)) continue;

    cells[y][x].hasBomb = true;

    if(x+1 < FIELD_WIDTH) cells[y][x+1].number++;
    if(x-1 >= 0) cells[y][x-1].number++;
    if(y+1 < FIELD_HEIGHT) cells[y+1][x].number++;
    if(y-1 >= 0) cells[y-1][x].number++;

    if(x+1 < FIELD_WIDTH && y+1 < FIELD_HEIGHT) cells[y+1][x+1].number++;
    if(x-1 >= 0 && y+1 < FIELD_HEIGHT) cells[y+1][x-1].number++;
    if(x+1 < FIELD_WIDTH && y-1 >= 0) cells[y-1][x+1].number++;
    if(x-1 >= 0 && y-1 >= 0) cells[y-1][x-1].number++;

    count--;
  };

  isClicked = true;
};

function InitGameField(field, cells){

  let fragment = document.createDocumentFragment();

  for (let j = 0; j < FIELD_HEIGHT; j++) {
  
    let cellsRow = [];
  
    for (let i = 0; i < FIELD_WIDTH; i++) {
  
      let cell = document.createElement("div");
      cell.classList.add("cell");
      cell.classList.add("cell-closed");
      cell.x = i;
      cell.y = j;    
      cell.hasBomb = false;
      cell.number = 0;
  
      cellsRow.push(cell);
  
      fragment.appendChild(cell);    
    }
  
    cells.push(cellsRow);
  }
  
  field.style.width = FIELD_WIDTH * 30 + "px";
  field.style.height = FIELD_HEIGHT * 30 + "px";
  
  field.appendChild(fragment);

};